#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define MAX_THREADS 12
const long num_steps = 100000000;
double step;

int main(void)
{
  int j;
  double pi;
  double start_time, run_time;
  for (j = 1; j <= MAX_THREADS; j++)
  {
    start_time = omp_get_wtime();
    double sum = 0.0;
#pragma omp parallel num_threads(j)
    {
      double partial_sum = 0.0;
#pragma omp for
      for (int k = 0; k<num_steps; k++)
        partial_sum += (k % 2 ? -1.0 : 1.0) / (2 * k + 1);
#pragma omp critical
      sum += partial_sum;
    }
    run_time = omp_get_wtime() - start_time;
    pi = 4.0*sum;
    printf("pi is %f in %f seconds and %d threads\n", pi, run_time, j);
  }

  system("pause");
  return 0;
}