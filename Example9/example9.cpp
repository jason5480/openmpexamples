#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define MAX_THREADS 12
const long num_steps = 100000000;
double step;

int main(void)
{
  int j;
  double pi;
  double start_time, run_time;
  double partial_sum[MAX_THREADS];
  for (j = 1; j <= MAX_THREADS; j++)
  {
    start_time = omp_get_wtime();
    double factor = 1.0;
    double sum = 0.0;
#pragma omp parallel num_threads(j)
    {
      int i = omp_get_thread_num();
      partial_sum[i] = 0.0;
#pragma omp for
      for (int k = 0; k < num_steps; k++)
      {
        partial_sum[i] += factor / (2 * k + 1);
        // should be atomic too?
        factor = -factor;
      }
#pragma omp atomic
      sum += partial_sum[i];
    }
    run_time = omp_get_wtime() - start_time;
    pi = 4.0*sum;
    printf("pi is %f in %f seconds and %d threads\n", pi, run_time, j);
  }

  system("pause");
  return 0;
}