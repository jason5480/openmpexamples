#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main()
{
  printf("Hello world parallel program\n");
#pragma omp parallel
  {
    int tid = omp_get_thread_num();
    int total = omp_get_num_threads();
    printf("Hello world from thread %d of %d\n", tid, total);
  }

  system("pause");
  return 0;
}