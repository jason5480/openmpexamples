#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main()
{
  printf("deadlock example\n");
#pragma omp parallel
  {
    int tid = omp_get_thread_num();
    int total = omp_get_num_threads();
#pragma omp single
    printf("Total threads %d\n", total);
#pragma omp for
    for (int i = 0; i < 10; i++)
    {
      printf("Iteration %d is assigned to thread %d\n", i, tid);
      // some calculation
      // ....
    }
#pragma omp barrier
  }

  system("pause");
  return 0;
}