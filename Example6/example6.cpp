#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main()
{
  printf("shared and private clauses example\n");
  int sum;
#pragma omp parallel private(sum)
  {
    int tid = omp_get_thread_num();
    int total = omp_get_num_threads();
#pragma omp master
      printf("Total threads %d\n", total);
    sum = 0;
    for (int i = 0; i<10000; i++)
    {
      sum += i;
    }
    printf("Thread %d: sum=%d\n", tid, sum);
  }
  printf("Correct : sum=%d\n", 10000 * 9999 / 2);

  system("pause");
  return 0;
}