#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main()
{
  printf("for construct example\n");
#pragma omp parallel
  {
    int tid = omp_get_thread_num();
    int total = omp_get_num_threads();
    printf("This is thread %d of %d\n", tid, total);
#pragma omp for
    for (int i = 0; i<20; i++)
    {
      printf("Iteration %d is assigned to thread %d\n", i, tid);
      // some calculation
      // ....
    }
  }

  system("pause");
  return 0;
}