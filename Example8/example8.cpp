#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main()
{
  printf("barrier synchronization example\n");
  printf("\nWithout barrier\n");
#pragma omp parallel
  {
    int tid = omp_get_thread_num();
    printf("Before. This is thread %d\n", tid);
    printf("After. This is thread %d\n", tid);
  }
  printf("\nWith barrier\n");
#pragma omp parallel
  {
    int tid = omp_get_thread_num();
    printf("Before. This is thread %d\n", tid);
#pragma omp barrier
    printf("After. This is thread %d\n", tid);
  }

  system("pause");
  return 0;
}