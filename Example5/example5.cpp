#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main()
{
  printf("single & master construct example\n");
#pragma omp parallel
  {
    int tid = omp_get_thread_num();
    int total = omp_get_num_threads();
    printf("This is thread %d of %d\n", tid, total);
#pragma omp for
    for (int i = 0; i<5; i++)
    {
      printf("Iteration %d of 1st loop is assigned to thread %d\n", i, tid);
      // some calculation
      // ....
    }
#pragma omp single
    printf("This (single constr.) is executed by thread %d\n", tid);
#pragma omp master
    printf("This (master constr.) is executed by thread %d\n", tid);
#pragma omp for
    for (int i = 0; i<5; i++)
    {
      printf("Iteration %d of 2nd loop is assigned to thread %d\n", i, tid);
      // some calculation
      // ....
    }
  }

  system("pause");
  return 0;
}