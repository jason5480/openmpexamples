#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main()
{
  printf("section construct example\n");
#pragma omp parallel
  {
    int tid = omp_get_thread_num();
    int total = omp_get_num_threads();
    printf("This is thread %d of %d\n", tid, total);
#pragma omp sections
    {
#pragma omp section
      {
        printf("1st Section is assigned to thread %d\n", tid);
        // some calculation
        // ....
      }
#pragma omp section
      {
        printf("2nd Section is assigned to thread %d\n", tid);
        // some other calculation
        // ....
      }
#pragma omp section
      {
        printf("3rd Section is assigned to thread %d\n", tid);
        // some other calculation
        // ....
      }
#pragma omp section
      {
        printf("4th Section is assigned to thread %d\n", tid);
        // some other calculation
        // ....
      }
    }
  }

  system("pause");
  return 0;
}