#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

#define ACCOUNTS 4

bool trans(double *d1, double *d2, double v)
{
  if (*d1 >= v)
  {
    *d1 -= v;
    *d2 += v;
    return true;
  }
  return false;
}

void printsum(double *vals)
{
  double sum = 0.0;
  for (int i = 0; i<ACCOUNTS; i++)
    sum += vals[i];
  printf("Total is %.2f\n", sum);
}

int main()
{
  double accvals[ACCOUNTS];
  printf("transactions example\n");
  for (int i = 0; i<ACCOUNTS; i++){
    accvals[i] = 1000.0;
    printf("Account %d contains %.2f\n", i, accvals[i]);
  }
  printsum(accvals);
  srand(time(NULL));
  printf("Running random transactions...\n");
  double start_time = omp_get_wtime();
#pragma omp parallel for
    for (int i = 0; i<1000000; i++)
    {
      int i1 = rand() % ACCOUNTS;
      int i2 = rand() % (ACCOUNTS - 1);
      if (i2 == i1) i2++;
      double v = (rand() % 100000) / 100.0;
#pragma omp critical
      trans(&accvals[i1], &accvals[i2], v);
    }
  printf("Total time %.3f, %d threads\n", omp_get_wtime() - start_time, omp_get_max_threads());
  for (int i = 0; i<ACCOUNTS; i++)
    printf("Account %d contains %.2f\n", i, accvals[i]);
  printsum(accvals);

  system("pause");
  return 0;
}